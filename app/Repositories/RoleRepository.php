<?php

namespace App\Repositories;
use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }

    public function updated(array $data, $id)
    {
        $role = $this->find($id);
        $role->update($data);
        $role->syncPermision($data['permissions']);
        return $role;
    }


    public function deleted($id)
    {
        $role = $this->find($id);
        $role->delete();
        $role->dettachPermision();
        return $role;
    }
}
