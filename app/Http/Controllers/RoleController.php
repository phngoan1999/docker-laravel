<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $permissionService;
    protected $roleService;

    public function __construct(PermissionService $permissionService,RoleService $roleService)
    {
        $this->permissionService = $permissionService;
        $this->roleService = $roleService;
    }

    public function index()
    {
        $roles = $this->roleService->all();
        return view('roles.index',compact('roles'));
    }

    public function create()
    {
        $permissions = $this->permissionService->all();
        return view('roles.create',compact('permissions'));
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->store($request);
        return redirect()->route('roles.index')->with('message', 'Save success');
    }

    public function show($id)
    {
        $role = $this->roleService->find($id);
        return view('roles.show',compact('role'));
    }

    public function edit($id)
    {
        $role = $this->roleService->find($id);
        $permissions = $this->permissionService->all();
        return view('roles.edit',compact('role','permissions'));
    }

    public function update(RoleRequest $request,$id)
    {
        $this->roleService->update($request,$id);
        return redirect()->route('roles.index')->with('message', 'Update success');
    }

    public function delete($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index')->with('message', 'Delete success');
    }
}
