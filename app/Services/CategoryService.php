<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use Illuminate\Support\MessageBag;
use function PHPUnit\Framework\throwException;

class CategoryService
{
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function store($request)
    {
        return $this->categoryRepository->create($request);
    }

    public function find($id)
    {
        return $this->categoryRepository->findOrFail($id,'products');
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $category = $this->categoryRepository->find($id);
        return $category->update($dataUpdate);
    }

    public function delete($id)
    {
        $category = $this->categoryRepository->delete($id);
        return $category;
    }
}
