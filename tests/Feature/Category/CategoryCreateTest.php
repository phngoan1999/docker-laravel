<?php

namespace Tests\Feature\Category;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryCreateTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_see_create_category_test()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get('/Categories');
        $response->assertViewIs('categories.index');
        $response->assertSee('Catogory Management');
        $response->assertStatus(200);
    }
}
