@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Management</h4>
                        <a href="{{ route('users.create') }}" class="btn btn-primary">Create user</a>
                        <form action="{{ route('users.index') }}" method="GET">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="name" placeholder="name"/>
                                </div>
                                <div class="col-md-3">
                                    <select class="js-example-basic-single w-100" name="role_id">
                                        <option value="">Select Role</option>
                                        @foreach($roles as $iterm)
                                            <option value="{{ $iterm->id }}">{{ $iterm->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-info">Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive pt-3">
                            <p style="display: none;">{{ $stt = 0 }}</p>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($users))
                                    @foreach($users as $iterm)
                                        <tr>
                                            <td>{{ $stt = $stt + 1 }}</td>
                                            <td>{{ $iterm->name }}</td>
                                            <td>{{ $iterm->email }}</td>
                                            <form action="{{route('users.delete',$iterm->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <td>
                                                    @hasPermission('user-delete')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                    @endhasPermission
                                                    @hasPermission('user-show')
                                                    <a href="{{ route('users.show',$iterm->id) }}" class="btn btn-warning">Detail</a>
                                                    @endhasPermission
                                                    @hasPermission('user-edit')
                                                    <a href="{{ route('users.edit',$iterm->id) }}" class="btn btn-warning">Edit</a>
                                                    @endhasPermission
                                                </td>
                                            </form>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
