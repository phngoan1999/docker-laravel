<?php
if(!function_exists('show_categories'))
{
    function show_categories($categories, $categoriesSelected = [], $parentId = null, $char = '')
    {
        foreach ($categories as $key => $category)
        {
            if ($category->parent_id === $parentId)
            {
                $selected = null;
                if(!empty($categoriesSelected))
                {
                    if(in_array($category->id, $categoriesSelected))
                    {
                        $selected = 'selected';
                    }
                }
                echo '<option value="'.$category->id.'" '.$selected.'>'.$char.$category->name.'</option>';
                unset($categories[$key]);
                show_categories($categories, $categoriesSelected, $category->id, $char.'--');
            }
        }
    }
}

if(!function_exists('format_price'))
{
    function format_price($price, $currency = 'đ')
    {
        return number_format($price, 0, '', '.') . $currency;
    }
}

if(!function_exists('format_description'))
{
    function format_description($description)
    {
        if(strlen($description) > 35)
        {
            return substr($description, 0, 35) . '...';
        }
        return $description;
    }
}
