<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use Illuminate\Http\Request;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function store($request)
    {
        $role = $this->roleRepository->create($request->all());
        $role->onattach($request->permissions);
        return $role;
    }

    public function find($id)
    {
        $role = $this->roleRepository->find($id);
        return $role;
    }

    public function update($request,$id)
    {
        $role = $this->roleRepository->updated($request->all(),$id);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->deleted($id);
        return $role;
    }
}
