@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <h4 class="card-title">Product Management</h4>
                        </div>
                        <div class="row">
                            @hasPermission('products-store')
                            <button type="button" class="btn btn-success" data-toggle="modal" id="create-modal-view" data-url-create="{{route('products.create')}}">
                                <i class="fa fa-fw fa-edit"></i>
                                Thêm Sản phẩm
                            </button>
                            @endhasPermission
                        </div>
                        <br>
                        <form action="{{route('products.index')}}" method="GET">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Name" name="namesearch">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Category" name="categorysearch">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive pt-3">
                            <div style="display: none;">{{$stt = 0}}</div>
                            <table class="table table-bordered" id="product-table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Ảnh mô tả</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody >
                                    @foreach($products as $product)
                                        <tr>
                                            <th>{{ $stt=$stt+1 }}</th>
                                            <th>{{ $product->name }}</th>
                                            <th>
                                                <img src="{{ asset('uploads/products/'.$product->image) }}"  width="50px"alt="">
                                            </th>
                                            <th>
                                                @hasPermission('products-find')
                                                <button type="button" class="btn btn-success show-modal-product"  value="{{$product->id}}" data-url-find="{{route('products.find',$product->id)}}">Show</button>
                                                @endhasPermission
                                                @hasPermission('products-delete')
                                                <button type="button" class="btn btn-danger delete-modal-product" value="{{$product->id}}" data-url-delete="{{route('products.delete',$product->id)}}" >Delete</button>
                                                @endhasPermission
                                                @hasPermission('products-edit')
                                                <button type="button" class="btn btn-success edit-modal-product"  value="{{$product->id}}" data-url-find="{{route('products.edit',$product->id)}}">Update</button>
                                                @endhasPermission
                                            </th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--  Modal form add  --}}
    <div class="modal fade" id="createProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="append-html-create">

                </div>
            </div>
        </div>
    </div>


    {{--  Modal form show  --}}
    <div class="modal fade" id="showProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="show-product">

            </div>
        </div>
    </div>


    {{--  Modal form update  --}}
    <div class="modal" id="updateProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" id="update-product-view">

            </div>
        </div>
    </div>

    {{--  Modal form delete  --}}
    <div class="modal" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="form-delete">
                    <div class="modal-header">
                        <h5 class="modal-title">Xóa sản phẩm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Bạn chắc chắn muốn xóa không?</p>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="delete-product">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="module" src="{{asset('js/product.js')}}"></script>

    <style>
        .error{
            color: red;
        }
    </style>
@endsection



