<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    protected $categoryservice;

    public function __construct(CategoryService $categoryservice,ProductService $productService)
    {
        $this->categoryservice = $categoryservice;
        $this->productService = $productService;

        View::share('categories', $this->categoryservice->all());
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);
        return view('products.index',compact('products'));
    }

    public function create()
    {
        return view('products.create');
    }

    public function list()
    {
        $products = $this->productService->all();
        return view('products.table',compact('products'));
    }

    public function store(ProductRequest $request)
    {
        $this->productService->create($request);
        $productList = $this->list();
        return $productList;
    }

    public function find($id)
    {
        $product = $this->productService->find($id);
        return view('products.show',compact('product'));
    }

    public function edit($id)
    {
        $product = $this->productService->find($id);
        return view('products.edit',compact('product'));
    }

    public function update(ProductUpdateRequest $request,$id)
    {
        $this->productService->update($request,$id);
        $productList = $this->list();
        return $productList;
    }

    public function delete($id)
    {
        $this->productService->delete($id);
        $productList = $this->list();
        return $productList;
    }
}
