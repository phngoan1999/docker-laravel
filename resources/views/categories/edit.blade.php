@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category Management</h4>
                        <form action="{{route('categories.update',$category->id)}}" method="POST" class="form-sample">
                            @csrf
                            @method('PUT')
                            <p class="card-description">
                                Category info
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" placeholder="Name ..." value="{{$category->name}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Slug</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="slug"  placeholder="Slug.." value="{{$category->slug}}"/>
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-6">
                                   <div class="form-group row">
                                       <label class="col-sm-3 col-form-label">Category</label>
                                       <div class="col-sm-9">
                                           <select class="js-example-basic-multiple w-100" name="parent_id">
                                               <option value="" disabled selected>---chọn danh mục---</option>
                                               @foreach($categories as $iterm)
                                                   <option  {{ $category->parent_id == $iterm->id ? 'selected' : ''}}  value="{{ $iterm->id }}">{{ $iterm->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
