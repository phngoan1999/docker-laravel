<?php

namespace App\Services;
use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['namesearch'] = $request->namesearch;
        $dataSearch['categorysearch'] = $request->categorysearch;
        return $this->productRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function find($id)
    {
        return $this->productRepository->findOrFail($id, 'categories');
    }

    public function create($request)
    {
        $dataInsert = $request->all();
        $dataInsert['image'] = $this->storeImage($request);
        $product = $this->productRepository->create($dataInsert);
        $product->addCategories($request->categories);
        return $product;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $product = $this->productRepository->find($id);
        $dataUpdate['image'] = $this->updateImage($request, $product->image);
        $product->update($dataUpdate);
        $product->syncCategories($request->categories);
        return $product;
    }

    public function delete($id)
    {
        $product = $this->productRepository->find($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }
}
