<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserUpdateTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function authenticate_admin_can_update_user()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataCreate = User::factory()->create();
        $dataUpdate = [
            'name'      => $this->faker->text,
            'email'     => $this->faker->email,
            'password'  => '123456',
            'role_id'   => ['2']
        ];
        $dataTest = [
            'name'      => $dataUpdate['name'],
            'email'     => $dataUpdate['email']
        ];
        $response = $this->put($this->getUpdateUserRouter($dataCreate->id),$dataUpdate);
        $this->assertDatabaseHas('users',$dataTest);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    public function getUpdateUserRouter($id)
    {
        return route('users.update',$id);
    }
}
