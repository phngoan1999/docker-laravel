<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;

        View::share('categories', $this->categoryService->all());
    }

    public function index()
    {
        return view('categories.index');
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(CategoryRequest $request)
    {
        $this->categoryService->store($request->all());
        return redirect()->route('categories.index')->with('message', 'Save success');
    }

    public function show($id)
    {
        $category = $this->categoryService->find($id);
        return view('categories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = $this->categoryService->find($id);
        $categories = $this->categoryService->all();
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request,$id)
    {
        $this->categoryService->update($request,$id);
        return redirect()->route('categories.index')->with('message', 'Update success');
    }

    public function delete($id)
    {
        $this->categoryService->delete($id);
        return redirect()->route('categories.index')->with('message', 'Delete success');
    }
}
