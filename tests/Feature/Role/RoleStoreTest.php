<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleStoreTest extends TestCase
{
    /** @test
     */
    public function authenticate_user_can_store_role()
    {
        $user = User::find(7);
        $this->actingAs($user);
        $dataCreate = [
            'name' => "admintest4",
            'permissions' => [2]
        ];
        $dataRole = ['name' => "admintest"];
        $countRoleBefore = Role::count();
        $response = $this->post($this->getStoreRoute(),$dataCreate);
        $response->assertRedirect($this->getIndexRoute());
        $this->assertDatabaseHas('roles',$dataRole);
        $countRoleAfter = Role::count();
        $this->assertEquals($countRoleBefore + 1, $countRoleAfter);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test
     */
    public function authenticate_user_can_not_store_role_if_name_null()
    {
        $user = User::find(7);
        $this->actingAs($user);
        $dataCreate = [
            'name' => "",
            'permissions' => [2]
        ];
        $response = $this->post($this->getStoreRoute(),$dataCreate);
        $response->assertSessionHasErrors(['name']);
    }

    public function getStoreRoute()
    {
        return route('roles.store');
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }
}
