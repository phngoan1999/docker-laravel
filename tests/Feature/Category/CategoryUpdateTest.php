<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CategoryUpdateTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function authenticate_user_can_update_category_if_data_is_validate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataCreate = Category::factory()->create();
        $dataUpdate = [
            'name' => 'ABC2',
            'slug' => $this->faker->text,
            'parent_id' => 2
        ];
        $response = $this->put($this->getCategoryUpdateRoute($dataCreate->id),$dataUpdate);
        $this->assertDatabaseHas('categories',$dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    public function getCategoryUpdateRoute($id)
    {
        return route('categories.update',$id);
    }
}
