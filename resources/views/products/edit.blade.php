{{--  Modal form update  --}}
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form id="form-update">
    <div class="modal-body">
        <div class="form-group">
            <input type="text" class="form-control" id="nameUpdate" name="name" value="{{$product->name}}">
        </div>
        <div class="error" id="error-name"></div>
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="price"  placeholder="price" value="{{$product->price}}">
        </div>
        <div class="error" id="error-price"></div>
        <div class="form-group">
            @foreach($categories as $iterm)
                <div>
                    <input {{$product->categories->contains('id', $iterm->id) ? 'checked' : ''}}
                           type="checkbox" name="categories[]" value="{{$iterm->id}}">
                    <label>{{$iterm->name}}</label>
                </div>
            @endforeach
        </div>
        <div class="error" id="error-categories"></div>
        <div class="form-group">
            <input type="file"  class="form-control" name="image" id="imageUpdate">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" router-update-url="{{route('products.update',$product->id)}}" id="update-product" name="updateId" value="{{$product->id}}">Update</button>
    </div>
</form>
