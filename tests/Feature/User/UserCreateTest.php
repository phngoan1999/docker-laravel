<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserCreateTest extends TestCase
{
    /** @test */
    public function authenticate_admin_can_see_view_data_create()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get($this->getCreateUserRouter());
        $response->assertViewIs('users.create');
        $response->assertSee('User Management');
        $response->assertStatus(200);
    }
    public function getCreateUserRouter()
    {
        return route('users.create');
    }
}
