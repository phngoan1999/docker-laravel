@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category Management</h4>
                        <form action="" >
                            <p class="card-description">
                                Category info
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" placeholder="Name ..." value="{{$category->name}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Slug</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="slug"  placeholder="Slug.." value="{{$category->slug}}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Danh mục</label>
                                        @foreach($category->products as $iterm)
                                            <span class="label success">{{$iterm->name}}</span>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .label {
            color: white;
            padding: 8px;
            margin: 10px;
        }
        .success {background-color: #04AA6D;} /* Green */
    </style>
@endsection
