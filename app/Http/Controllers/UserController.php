<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected $roleService;
    protected $userService;

    public function __construct(RoleService $roleService,UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;

        View::share('roles', $this->roleService->all());
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view('users.index',compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(UserRequest $request)
    {
        $this->userService->store($request);
        return redirect()->route('users.index')->with('message', 'Save success');
    }

    public function show($id)
    {
        $user = $this->userService->find($id);
        return view('users.show',compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->find($id);
        return view('users.edit',compact('user'));
    }

    public function update(UserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index')->with('message', 'Update success');
    }

    public function delete($id)
    {
        $this->userService->delete($id);
        return redirect()->route('users.index')->with('message', 'Delete success');
    }
}
