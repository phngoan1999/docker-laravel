@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Roles Management</h4>
                        <div class="table-responsive pt-3">
                            <a href="{{ route('roles.create') }}" class="btn btn-primary">Create role</a>
                            <div style="display: none;">{{$stt = 0}}</div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Roles</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $iterm)
                                    <tr>
                                        <th scope="col">{{ $stt = $stt + 1  }}</th>
                                        <th scope="col">{{ $iterm->name  }}</th>
                                        <th scope="col">
                                            <form method="POST" action="{{route('roles.delete',$iterm->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                @hasPermission('roles-edit')
                                                <a href="{{ route('roles.edit',$iterm->id) }}" class="btn btn-warning" target="_blank">Edit</a>
                                                @endhasPermission
                                                @hasPermission('roles-show')
                                                <a href="{{ route('roles.show',$iterm->id) }}" class="btn btn-warning" target="_blank">Detail</a>
                                                @endhasPermission
                                                @hasPermission('roles-delete')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                @endhasPermission
                                            </form>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
