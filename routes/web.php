<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::middleware(['auth'])->group(function() {

    Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::name('products.')->prefix('Products')->group(function () {

        Route::get('/', [ProductController::class, 'index'])
            ->name('index')
            ->middleware('permission:products-view');

        Route::get('/create', [ProductController::class,'create'])
            ->name('create')
            ->middleware('permission:products-create');

        Route::post('/store', [ProductController::class,'store'])
            ->name('store')
            ->middleware('permission:products-store');

        Route::get('/show/{id}', [ProductController::class,'find'])
            ->name('find')
            ->middleware('permission:products-find');

        Route::get('/edit/{id}', [ProductController::class,'edit'])
            ->name('edit')
            ->middleware('permission:products-edit');

        Route::patch('/update/{id}', [ProductController::class,'update'])
            ->name('update')
            ->middleware('permission:products-update');

        Route::delete('/delete/{id}', [ProductController::class,'delete'])
            ->name('delete')
            ->middleware('permission:products-delete');

    });

    Route::name('roles.')->prefix('Roles')->group(function () {

        Route::get('/', [RoleController::class,'index'])
            ->name('index')
            ->middleware('permission:roles-view');

        Route::get('/create', [RoleController::class,'create'])
            ->name('create')
            ->middleware('permission:roles-create');

        Route::get('/show/{id}', [RoleController::class,'show'])
            ->name('show')
            ->middleware('permission:roles-show');

        Route::get('/edit/{id}', [RoleController::class,'edit'])
            ->name('edit')
            ->middleware('permission:roles-edit');

        Route::put('/update/{id}', [RoleController::class,'update'])
            ->name('update')
            ->middleware('permission:roles-update');

        Route::delete('/delete/{id}', [RoleController::class,'delete'])
            ->name('delete')
            ->middleware('permission:roles-delete');

        Route::post('/', [RoleController::class,'store'])
            ->name('store')
            ->middleware('permission:roles-store');

    });

    Route::name('users.')->prefix('users')->group(function () {

        Route::get('/', [UserController::class,'index'])
            ->name('index')
            ->middleware('permission:user-view');

        Route::get('/create', [UserController::class,'create'])
            ->name('create')
            ->middleware('permission:user-create');

        Route::post('/store', [UserController::class,'store'])
            ->name('store')
            ->middleware('permission:user-store');

        Route::get('/show/{id}', [UserController::class,'show'])
            ->name('show')
            ->middleware('permission:user-show');

        Route::get('/edit/{id}', [UserController::class,'edit'])
            ->name('edit')
            ->middleware('permission:user-edit');

        Route::put('/update/{id}', [UserController::class,'update'])
            ->name('update')
            ->middleware('permission:user-update');

        Route::delete('/delete/{id}', [UserController::class,'delete'])
            ->name('delete')
            ->middleware('permission:user-delete');

    });
    Route::name('categories.')->prefix('Categories')->group(function () {

        Route::get('/', [CategoryController::class,'index'])
            ->name('index')
            ->middleware('permission:categories-index');

        Route::get('/create', [CategoryController::class,'create'])
            ->name('create')
            ->middleware('permission:categories-create');

        Route::post('/store', [CategoryController::class,'store'])
            ->name('store')
            ->middleware('permission:categories-store');

        Route::get('/show/{id}', [CategoryController::class,'show'])
            ->name('show')
            ->middleware('permission:categories-show');

        Route::get('/edit/{id}', [CategoryController::class,'edit'])
            ->name('edit')
            ->middleware('permission:categories-edit');

        Route::put('/update/{id}', [CategoryController::class,'update'])
            ->name('update')
            ->middleware('permission:categories-update');

        Route::delete('/delete/{id}', [CategoryController::class,'delete'])
            ->name('delete')
            ->middleware('permission:categories-delete');

    });
});





