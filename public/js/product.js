
import base,{METHOD_GET, METHOD_POST, METHOD_PATCH, METHOD_DELETE} from "./base.js";

(function ($, window, document){
    $(function () {
        let array_error = new Array();

        //create
        $(document).on('click','#create-modal-view', function(){
            let url = $(this).attr('data-url-create');
            base.callApi(url)
                .done(function (response) {
                    $('#append-html-create').append(response);
                    $('#createProductModal').modal('show');
                })
                .fail(function (response) {
                    base.showMessage('error','create error');
                });
        });

        //show
        $(document).on('click','.show-modal-product', function(){
            let updateId = $(this).val();
            let url = $(this).attr('data-url-find');
            base.callApi(url, METHOD_GET, updateId)
                .done(function (response) {
                    $('#show-product').html(response);
                    $('#showProductModal').modal('show');
                })
                .fail(function (response) {
                    base.showMessage('error','show error');
                });
        });

        //show modal edit
        $(document).on('click','.edit-modal-product', function(){
            let updateId = $(this).val();
            let url = $(this).attr('data-url-find');
            base.callApi(url, METHOD_GET, updateId)
                .done(function (response) {
                    $('#update-product-view').html(response);
                    $('#updateProductModal').modal('show');
                })
                .fail(function (response) {
                    base.showMessage('error','updata error');
                });
        });

        //show confirm delete
        $(document).on('click','.delete-modal-product', function(){
            let deleteId = $(this).val();
            let delete_url = $(this).attr('data-url-delete');
            base.confirm()
                .then((result) => {
                    if (result.isConfirmed) {
                        base.callApiWithFormData(delete_url,METHOD_DELETE,deleteId)
                            .done(function (response) {
                                base.showMessage('success','delete success');
                                $('#product-table').html(response);
                            })
                            .fail(function (response) {
                                base.showMessage('error','delete error');
                            });

                    }
                })
        });

        //Update product
        $(document).on('click','#update-product', function(){
            let formData = new FormData($('#form-update')[0]);
            formData.append('_method', METHOD_PATCH);
            let url = $(this).attr('router-update-url');
            base.callApiWithFormData(url, METHOD_POST, formData)
                .done(function (response) {
                    base.showMessage('success','Update success');
                    $('#updateProductModal').modal('hide');
                    $('#product-table').html(response);
                })
                .fail(function (response) {
                    let errors = response.responseJSON.errors;
                    showerrormessage(errors);
                });
        });

        //create
        $(document).on('click','#add-product', function () {
            let formData = new FormData($('#form-create')[0]);
            let url = $(this).attr('data-url');
            base.callApiWithFormData(url, METHOD_POST, formData)
                .done(function (response) {
                    base.showMessage('success','Add success');
                    $('#createProductModal').modal('hide');
                    $('#product-table').html(response);
                })
                .fail(function (response) {
                    let errors = response.responseJSON.errors;
                    showerrormessage(errors);
                });
        });

        function showerrormessage(errors)
        {
            if(array_error.length>0){
                $.map(array_error, function(val) {
                    $("#error-"+val).html('');
                });
            }
            array_error = [];
            $.map(errors, function(val, key) {
                array_error.push(key);
                $("#error-"+key).html(val[0]);
            });
        }
    })
}(window.jQuery, window, document))
