<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Query\Builder;

//use Your Model

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }

    public function store(array $data)
    {
        $user = $this->create($data);
        $user->attachRole($data['role_id']);
        return $user;
    }
    public function updated(array $data,$id)
    {
        $user = $this->find($id);
        $user->update($data);
        $user->syncRole($data['role_id']);
        return $user;
    }

    public function delete($id)
    {
        $user = $this->find($id);
        $user->delete();
        $user->detachRole();
        return $user;
    }

    public function search(array $data)
    {
        return $this->model->withName($data['name'] ?? null)
            ->withRoleId($data['role_id'] ?? null)
            ->get();
    }
}
