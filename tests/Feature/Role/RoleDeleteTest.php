<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleDeleteTest extends TestCase
{
    use WithFaker;
    /** @test
     */
    public function authenticate_user_can_delete_role()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataRole = Role::factory()->create();
        $countBefore = Role::count();
        $response = $this->delete($this->getDeleteRoute($dataRole->id));
        $countAfter = Role::count();
        $this->assertEquals($countBefore,$countAfter + 1);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    public function getDeleteRoute($id)
    {
        return route('roles.delete',$id);
    }
}
