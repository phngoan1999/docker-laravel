@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Category Management</h4>
                        <form action="{{route('categories.store')}}" method="POST" class="form-sample">
                            @csrf
                            <p class="card-description">
                                Category info
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" placeholder="Name ..."/>
                                        </div>
                                    </div>
                                    @error('name')
                                    <div  style="color: red;">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Note</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="slug"  placeholder="note.."/>
                                        </div>
                                    </div>
                                    @error('slug')
                                    <div  style="color: red;">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Danh mục</label>
                                        <div class="col-sm-9">
                                            <select class="js-example-basic-multiple w-100" name="parent_id">
                                                <option value="" disabled selected>---chọn danh mục---</option>
                                                @foreach($categories as $iterm)
                                                    <option value="{{ $iterm->id }}">{{ $iterm->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
