@extends('Layouts.app')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Catogory Management</h4>
                        <a href="{{ route('categories.create') }}" class="btn btn-primary">Create Catogory</a>
                        <div style="display: none;">{{$stt = 0}}</div>
                        <div class="table-responsive pt-3">
                            <div style="display: none;">{{$stt = 0}}</div>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên danh mục</th>
                                    <th>Danh mục con</th>
                                    <th>Slug</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$stt = $stt + 1}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>
                                            @if(count($category->childrens) > 0)
                                                @foreach($category->childrens as $child)
                                                    {{$child->name}},
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{$category->slug}}</td>
                                        <form action="{{route('categories.delete',$category->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <td>
                                                @hasPermission('categories-delete')
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                @endhasPermission
                                                @hasPermission('categories-show')
                                                <a href="{{ route('categories.show',$category->id) }}" class="btn btn-warning">Detail</a>
                                                @endhasPermission
                                                @hasPermission('categories-update')
                                                <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-warning">Edit</a>
                                                @endhasPermission
                                            </td>
                                        </form>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
