<div style="display: none;">{{$stt = 0}}</div>
<thead>
<tr>
    <th>No.</th>
    <th>Tên sản phẩm</th>
    <th>Ảnh mô tả</th>
    <th></th>
</tr>
</thead>
<tbody >
@foreach($products as $product)
    <tr>
        <th>{{ $stt=$stt+1 }}</th>
        <th>{{ $product->name }}</th>
        <th>
            <img src="{{ asset('uploads/products/'.$product->image) }}"  width="50px" alt="">
        </th>
        <th>
            @hasPermission('products-find')
            <button type="button" class="btn btn-success show-modal-product"  value="{{$product->id}}" data-url-find="{{route('products.find',$product->id)}}">Show</button>
            @endhasPermission
            @hasPermission('products-delete')
            <button type="button" class="btn btn-danger delete-modal-product" value="{{$product->id}}" data-url-delete="{{route('products.delete',$product->id)}}" >Delete</button>
            @endhasPermission
            @hasPermission('products-edit')
            <button type="button" class="btn btn-success edit-modal-product"  value="{{$product->id}}" data-url-find="{{route('products.edit',$product->id)}}">Update</button>
            @endhasPermission</th>
    </tr>
@endforeach
</tbody>
