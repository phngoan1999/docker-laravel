<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CategoryStoreTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function authenticate_user_can_store_category_if_data_is_validate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataCreate = [
            'name' => 'ABC',
            'slug' => $this->faker->text,
            'parent_id' => 1
        ];
        $countBefore = Category::count();
        $response = $this->post($this->getCategoryStoreRoute(),$dataCreate);
        $countAfter = Category::count();
        $this->assertEquals($countBefore + 1,$countAfter);
        $this->assertDatabaseHas('categories',$dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function authenticate_user_can_not_store_category_if_name_is_capitalization()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataCreate = [
            'name' => 'adhsdj',
            'slug' => $this->faker->text,
            'parent_id' => 1
        ];
        $response = $this->post($this->getCategoryStoreRoute(),$dataCreate);
        $response->assertSessionHasErrors('name');
    }
    public function getCategoryStoreRoute()
    {
        return route('categories.store');
    }
}
