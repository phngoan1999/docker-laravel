<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleUpdateTest extends TestCase
{
    use WithFaker;
    /** @test
     */
    public function authenticate_user_can_update_role()
    {
        $user = User::find(7);
        $this->actingAs($user);
        $dataRole = Role::factory()->create();
        $dataUpdate = [
            'name' => "admintest5",
            'permissions' => [3]
        ];
        $data = ['name' => "admintest5"];
        $response = $this->put($this->getUpdateRoute($dataRole->id),$dataUpdate);
        $this->assertDatabaseHas('roles',$data);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test
     */
    public function authenticate_user_can_not_update_role_if_name_is_null()
    {
        $user = User::find(7);
        $this->actingAs($user);
        $dataRole = Role::factory()->create();
        $dataUpdate = [
            'name' => "",
            'permissions' => [3]
        ];
        $response = $this->put($this->getUpdateRoute($dataRole->id),$dataUpdate);
        $response->assertSessionHasErrors(['name']);
    }
    public function getUpdateRoute($id)
    {
        return route('roles.update',$id);
    }

    public function getIndexRoute()
    {
        return route('roles.index');
    }
}
