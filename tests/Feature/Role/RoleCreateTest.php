<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleCreateTest extends TestCase
{
    /** @test
     */
    public function authenticate_user_can_see_create_role()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get('/Roles');
        $response->assertViewIs('roles.index');
        $response->assertSee('Roles Management');
        $response->assertStatus(200);
    }
}
