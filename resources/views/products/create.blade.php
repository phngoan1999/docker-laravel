<form id="form-create">
    <div class="modal-body">
        <div class="form-group">
            <input type="text" class="form-control" name="name"  placeholder="name">
        </div>
        <div class="error" id="error-name"></div>
        <div class="form-group">
            <input type="text" class="form-control" id="price" name="price"  placeholder="price">
        </div>
        <div class="error" id="error-price"></div>
        <div class="form-group">
            @foreach($categories as $iterm)
                <div>
                    <input type="checkbox" name="categories[]" value="{{$iterm->id}}">
                    <label>{{$iterm->name}}</label>
                </div>
            @endforeach
        </div>
        <div class="error" id="error-categories"></div>
        <div class="form-group">
            <input type="file"  class="form-control" name="image">
        </div>
        <div class="error" id="error-image"></div>
    </div>

    <div id="error-name"></div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="add-product" data-url="{{ route('products.store') }}">Create</button>
    </div>
</form>
