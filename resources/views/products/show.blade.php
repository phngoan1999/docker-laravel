
<script src="{{asset('css/Layouts/Admin/vendors/select2/select2.min.js')}}"></script>
{{--  Modal form update  --}}
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form>
        <div class="modal-body">
            <div class="form-group">
                <input type="text" class="form-control" id="nameUpdate" name="name" value="{{$product->name}}">
            </div>
            <div class="form-group">
                @foreach($categories as $iterm)
                    <div>
                        <input {{$product->categories->contains('id', $iterm->id) ? 'checked' : ''}}
                               type="checkbox" name="categories[]" value="{{$iterm->id}}">
                        <label>{{$iterm->name}}</label>
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                <input type="file"  class="form-control" name="image" id="imageUpdate" value="{{$product->image}}">
            </div>
            <img src="{{ asset('uploads/products/'.$product->image) }}"  width="100px"alt="">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </form>
