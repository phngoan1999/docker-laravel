<?php

namespace App\Repositories;

abstract class BaseRepository
{
    public $model;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @throws BindingResolutionException
     */
    public function makeModel()
    {
        $this->model = app()->make($this->model());
    }

    abstract public function model();

    public function all()
    {
        return $this->model->all();
    }

    public function findOrFail($id, $table)
    {
        return $this->model->with($table)->findOrFail($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($dataInsert)
    {
        return $this->model->create($dataInsert);
    }

    public function update($dataUpdate, $id)
    {
        return $this->model->findOrFail($id)->update($dataUpdate);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function multipleDelete(array $ids)
    {
        return $this->model->destroy(array_values($ids));
    }

    public function count()
    {
        return $this->model->all()->count();
    }

    public function latest($id)
    {
        return $this->model->latest($id);
    }
}
