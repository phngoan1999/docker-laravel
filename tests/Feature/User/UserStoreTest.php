<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserStoreTest extends TestCase
{
    use WithFaker;
    /** @test */
    public function authenticate_admin_can_store_user()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataPost = [
            'name'      => $this->faker->text,
            'email'     => $this->faker->email,
            'password'  => '123456',
            'role_id'   => ['2']
        ];
        $countUserBefore = User::count();
        $response = $this->post($this->getStoreUserRouter(),$dataPost);
        $countUserAfter = User::count();
        $this->assertEquals($countUserBefore + 1, $countUserAfter);
        $response->assertStatus(Response::HTTP_FOUND);
    }
    /** @test */
    public function authenticate_admin_can_not_store_user_if_name_is_null()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $dataPost = [
            'name'      => '',
            'email'     => $this->faker->email,
            'password'  => '123456',
            'role_id'   => ['2']
        ];
        $response = $this->post($this->getStoreUserRouter(),$dataPost);
        $response->assertSessionHasErrors('name');
    }
    public function getStoreUserRouter()
    {
        return route('users.store');
    }
    public function getIndexUserRouter()
    {
        return route('users.index');
    }
}
