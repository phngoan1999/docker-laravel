<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Roles;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function store($request)
    {
        $data = $this->formatData($request);
        return $this->userRepository->store($data);
    }

    public function update($request,$id)
    {
        $data = $this->formatData($request);
        return $this->userRepository->updated($data, $id);
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function search($request)
    {
        return $this->userRepository->search($request->all());
    }

    public function formatData(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        return $data;
    }

    public function find($id)
    {
        return $this->userRepository->findOrFail($id,'roles');
    }
}
