export const METHOD_GET = "GET";
export const METHOD_POST = "POST";
export const METHOD_PATCH = "PATCH";
export const METHOD_DELETE = "DELETE";

function setupajax()
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

const base = {
    callApiWithFormData: function callApiWithFormData( url, method, data){
        setupajax();
        return $.ajax({
            url: url,
            type: method,
            data: data,
            contentType: false,
            processData: false,
        })
    },
    callApi: function callApi(url, method = METHOD_GET, data= null){
        setupajax();
        return $.ajax({
            url: url,
            type: method,
            data: data
        })
    },
    confirm: function (){
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        });
    },
    showMessage: function showMessage(status, title)
    {
        Swal.fire({
            position: 'top-end',
            icon: status,
            title: title,
            showConfirmButton: false,
            timer: 1500
        })
    }
};

export default base;
