<?php

namespace App\Repositories;
use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        // TODO: Implement model() method.
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['namesearch'])
            ->withCategoryName($dataSearch['categorysearch'])
            ->get();
    }
}
