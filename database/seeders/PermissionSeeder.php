<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'products-view',
            'products-create',
            'products-store',
            'products-find',
            'products-edit',
            'products-update',
            'products-delete',
            'roles-index',
            'roles-create',
            'roles-show',
            'roles-edit',
            'roles-update',
            'roles-delete',
            'roles-store',
            'user-view',
            'user-create',
            'user-store',
            'user-show',
            'user-edit',
            'user-update',
            'user-delete',
            'categries-index',
            'categries-create',
            'categries-store',
            'categries-edit',
            'categries-update',
            'ategries-delete',
        ];

        foreach ($permissions as $permission){
            Permission::create([
                'name' => $permission
            ]);
        }
    }
}
