<?php

namespace App\Services;

use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PermissionService
{
    protected $permissionService;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->permissionRepository->all();
    }
}
