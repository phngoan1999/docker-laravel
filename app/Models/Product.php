<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "products";
    protected $fillable = ['name','image','price'];


    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'product_category',
            'product_id',
            'category_id'
        );
    }

    public function addCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }

    public function syncCategories($categoryIds)
    {
        return $this->categories()->sync($categoryIds);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%'.$name.'%') : null;
    }

    public function scopeWithCategoryName($query, $name)
    {
        return $name ? $query->WhereHas('categories', function ($query) use ($name) {
            $query->where('name', $name);
        }): null;
    }
}
