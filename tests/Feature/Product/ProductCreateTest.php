<?php

namespace Tests\Feature\Product;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductCreateTest extends TestCase
{
    /** @test
     */
    public function authenticate_user_can_see_create_product()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get('/Products');
        $response->assertViewIs('products.index');
        $response->assertSee('Product Management');
        $response->assertStatus(200);
    }
}
